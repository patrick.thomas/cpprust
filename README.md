This is a minimal example of using CXX to call Rust code from C++.
The code is taken from https://stackoverflow.com/questions/71097948/failing-to-use-cxx-to-link-rust-written-library-in-c-project.

Run `cargo build` and then `g++ -o cpp_program src/main.cpp -I target/cxxbridge -L target/debug -l cpprust`.
